class CreateEnvoyes < ActiveRecord::Migration[7.0]
  def change
    create_table :envoyes do |t|
      t.datetime :date
      t.references :user, null: false, foreign_key: true
      t.integer :de
      t.string :a
      t.string :cc
      t.string :cci
      t.string :objet
      t.boolean :brouillon
      t.boolean :corbeille
      t.text :message

      t.timestamps
    end
  end
end
