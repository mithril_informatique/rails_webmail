class AddLuToRecu < ActiveRecord::Migration[7.0]
  def change
    add_column :recus, :lu, :boolean, default: false
  end
end
