// Entry point for the build script in your package.json
import "@hotwired/turbo-rails"
import "./controllers"
import * as bootstrap from "bootstrap"
import "trix"
import "@rails/actiontext"

document.addEventListener("turbo:load", () => {
  var select = document.querySelector('#envoye_a');
  if (select) {
    dselect(select);
  }
  document.querySelector(".trix-button-group--history-tools").hidden = true;
  document.querySelector(".trix-button--icon-bold").title = "Gras";
  document.querySelector(".trix-button--icon-italic").title = "Italique";
  document.querySelector(".trix-button--icon-strike").title = "Barré";
  document.querySelector(".trix-button--icon-link").title = "Lien";
  document.querySelector(".trix-button--icon-heading-1").title = "Titre";
  document.querySelector(".trix-button--icon-quote").hidden = true;
  document.querySelector(".trix-button--icon-bullet-list").title = "Liste à puces";
  document.querySelector(".trix-button--icon-number-list").title = "Liste numérotée";
  document.querySelector(".trix-button--icon-attach").hidden = true;
});
