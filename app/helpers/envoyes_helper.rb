module EnvoyesHelper
  def create_recu(envoye)
    pjs = @envoye.pj.map {|pj| {filename: pj.filename.to_s, link: url_for(pj)} }
    envoye.a.split(',').each do |a|
      Recu.create date: envoye.date, 
                  user_id: a.to_i, 
                  de: envoye.de, 
                  a: envoye.a, 
                  cc: envoye.cc, 
                  cci: envoye.cci.split(',').include?(a) ? a.to_i : '', 
                  objet: envoye.objet, 
                  contenu: envoye.contenu,
                  pj: pjs
    end
  end
end
