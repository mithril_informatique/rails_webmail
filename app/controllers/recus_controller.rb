class RecusController < ApplicationController
  before_action :set_recu, only: %i[ show edit update destroy ]
  before_action :permission, only: %i[ show edit update destroy ]

  # GET /recus or /recus.json
  def index
    @recus = Recu.where(user_id: @current_user_id).order("date DESC")
  end

  # GET /recus/1 or /recus/1.json
  def show
    unless @recu.lu
      @recu.lu = true
      @recu.save
      set_nb_non_lu
    end
    @pjs = @recu.pj ? eval(@recu.pj) : []
  end

  # GET /recus/new
  def new
    @recu = Recu.new
  end

  # GET /recus/1/edit
  def edit
  end

  # POST /recus or /recus.json
  def create
    @recu = Recu.new(recu_params)

    respond_to do |format|
      if @recu.save
        format.html { redirect_to recu_url(@recu), notice: "Recu was successfully created." }
        format.json { render :show, status: :created, location: @recu }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @recu.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /recus/1 or /recus/1.json
  def update
    respond_to do |format|
      if @recu.update(recu_params)
        format.html { redirect_to recu_url(@recu), notice: "Recu was successfully updated." }
        format.json { render :show, status: :ok, location: @recu }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @recu.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /recus/1 or /recus/1.json
  def destroy
    @recu.destroy

    respond_to do |format|
      format.html { redirect_to recus_url, notice: "Recu was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_recu
      @recu = Recu.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def recu_params
      params.require(:recu).permit(:date, :user_id, :de, :a, :cc, :cci, :objet, :corbeille, :message, pj: [])
    end

    def permission
      redirect_to recus_url, notice: "Interdit!" unless @recu.user_id==@current_user_id
    end
end
