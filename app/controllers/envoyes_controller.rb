class EnvoyesController < ApplicationController
  include EnvoyesHelper

  before_action :set_envoye, only: %i[ show edit update destroy ]
  before_action :set_users, only: %i[ new create edit update ]
  before_action :permission, only: %i[ show edit update destroy ]

  # GET /envoyes or /envoyes.json
  def index
    @envoyes = Envoye.where(user_id: @current_user_id).order("date DESC")
    @envoyes = @envoyes.where(brouillon: true) if params['brouillon']=='true'
  end

  # GET /envoyes/1 or /envoyes/1.json
  def show
  end

  # GET /envoyes/new
  def new
    @envoye = Envoye.new
    @users = @users.unshift([ "Sélectionnez un destinataire", "" ])
    if params['reponse']
      recu = Recu.find(params['reponse'].to_i)
      @envoye.a = [recu.de.to_s]
      @envoye.objet = "Re: #{recu.objet}"
      @envoye.contenu = "<br><br>-----#{recu.contenu}"
    end
  end

  # GET /envoyes/1/edit
  def edit
  end

  # POST /envoyes or /envoyes.json
  def create
    @envoye = Envoye.new(envoye_params)
    params['envoye']['a'].reject!(&:empty?)
    @envoye.a = params['envoye']['a'].join(',')
    @envoye.date = DateTime.now
    @envoye.user_id = @current_user_id
    @envoye.de = @current_user_id

    respond_to do |format|
      if @envoye.save
        create_recu(@envoye) unless @envoye.brouillon
        format.html { redirect_to envoyes_path, notice: "Envoye was successfully created." }
        format.json { render :show, status: :created, location: @envoye }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @envoye.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /envoyes/1 or /envoyes/1.json
  def update
    respond_to do |format|
      if @envoye.update(envoye_params)
        create_recu(@envoye) unless @envoye.brouillon
        format.html { redirect_to envoyes_path, notice: "Envoye was successfully updated." }
        format.json { render :show, status: :ok, location: @envoye }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @envoye.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /envoyes/1 or /envoyes/1.json
  def destroy
    @envoye.destroy

    respond_to do |format|
      format.html { redirect_to envoyes_url, notice: "Envoye was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_envoye
      @envoye = Envoye.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def envoye_params
      params.require(:envoye).permit(:date, :a, :cc, :cci, :objet, :brouillon, :corbeille, :contenu, pj: [])
    end

    def set_users
      @users = User.order(:prenom, :nom).collect {|u| [ u.nom_complet, u.id ] }
    end

    def permission
      redirect_to envoyes_url, notice: "Interdit!" unless @envoye.user_id==@current_user_id
    end
end
