class ApplicationController < ActionController::Base
  before_action :get_token
  before_action :set_current_user
  before_action :set_nb_non_lu

  private
    def set_current_user
      if session[:user]
        @current_user_id = session[:user]['id']
      else
        redirect_to '/', notice: "interdit!" 
      end     
    end

    def set_nb_non_lu
      @nb_non_lu = Recu.where(lu: false, user_id: @current_user_id).count
    end

    def get_token
      if params['token']
        user = User.where(token: params['token']).first
        session[:user] = user if user
      end
    end
end
