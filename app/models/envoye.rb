class Envoye < ApplicationRecord
  belongs_to :user
  has_many_attached :pj
  has_rich_text :contenu
  validates_presence_of :a

  def expediteur
    return User.find(self.de).nom_complet
  end

  def destinataires
    destinataires = []
    self.a&.split(',')&.each do |id|
      destinataires << User.find(id).nom_complet
    end
    return destinataires
  end
end
