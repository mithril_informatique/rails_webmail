class ApplicationRecord < ActiveRecord::Base
  primary_abstract_class
  has_rich_text :message
end
