json.extract! envoye, :id, :date, :user_id, :de, :a, :cc, :cci, :objet, :brouillon, :corbeille, :message, :pj, :created_at, :updated_at
json.url envoye_url(envoye, format: :json)
json.pj do
  json.array!(envoye.pj) do |pj|
    json.id pj.id
    json.url url_for(pj)
  end
end
