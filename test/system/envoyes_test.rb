require "application_system_test_case"

class EnvoyesTest < ApplicationSystemTestCase
  setup do
    @envoye = envoyes(:one)
  end

  test "visiting the index" do
    visit envoyes_url
    assert_selector "h1", text: "Envoyes"
  end

  test "should create envoye" do
    visit envoyes_url
    click_on "New envoye"

    fill_in "A", with: @envoye.a
    check "Brouillon" if @envoye.brouillon
    fill_in "Cc", with: @envoye.cc
    fill_in "Cci", with: @envoye.cci
    check "Corbeille" if @envoye.corbeille
    fill_in "Date", with: @envoye.date
    fill_in "De", with: @envoye.de
    fill_in "Message", with: @envoye.message
    fill_in "Objet", with: @envoye.objet
    fill_in "User", with: @envoye.user_id
    click_on "Create Envoye"

    assert_text "Envoye was successfully created"
    click_on "Back"
  end

  test "should update Envoye" do
    visit envoye_url(@envoye)
    click_on "Edit this envoye", match: :first

    fill_in "A", with: @envoye.a
    check "Brouillon" if @envoye.brouillon
    fill_in "Cc", with: @envoye.cc
    fill_in "Cci", with: @envoye.cci
    check "Corbeille" if @envoye.corbeille
    fill_in "Date", with: @envoye.date
    fill_in "De", with: @envoye.de
    fill_in "Message", with: @envoye.message
    fill_in "Objet", with: @envoye.objet
    fill_in "User", with: @envoye.user_id
    click_on "Update Envoye"

    assert_text "Envoye was successfully updated"
    click_on "Back"
  end

  test "should destroy Envoye" do
    visit envoye_url(@envoye)
    click_on "Destroy this envoye", match: :first

    assert_text "Envoye was successfully destroyed"
  end
end
