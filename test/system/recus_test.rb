require "application_system_test_case"

class RecusTest < ApplicationSystemTestCase
  setup do
    @recu = recus(:one)
  end

  test "visiting the index" do
    visit recus_url
    assert_selector "h1", text: "Recus"
  end

  test "should create recu" do
    visit recus_url
    click_on "New recu"

    fill_in "A", with: @recu.a
    fill_in "Cc", with: @recu.cc
    fill_in "Cci", with: @recu.cci
    check "Corbeille" if @recu.corbeille
    fill_in "Date", with: @recu.date
    fill_in "De", with: @recu.de
    fill_in "Message", with: @recu.message
    fill_in "Objet", with: @recu.objet
    fill_in "User", with: @recu.user_id
    click_on "Create Recu"

    assert_text "Recu was successfully created"
    click_on "Back"
  end

  test "should update Recu" do
    visit recu_url(@recu)
    click_on "Edit this recu", match: :first

    fill_in "A", with: @recu.a
    fill_in "Cc", with: @recu.cc
    fill_in "Cci", with: @recu.cci
    check "Corbeille" if @recu.corbeille
    fill_in "Date", with: @recu.date
    fill_in "De", with: @recu.de
    fill_in "Message", with: @recu.message
    fill_in "Objet", with: @recu.objet
    fill_in "User", with: @recu.user_id
    click_on "Update Recu"

    assert_text "Recu was successfully updated"
    click_on "Back"
  end

  test "should destroy Recu" do
    visit recu_url(@recu)
    click_on "Destroy this recu", match: :first

    assert_text "Recu was successfully destroyed"
  end
end
