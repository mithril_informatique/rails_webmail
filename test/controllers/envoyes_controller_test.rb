require "test_helper"

class EnvoyesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @envoye = envoyes(:one)
  end

  test "should get index" do
    get envoyes_url
    assert_response :success
  end

  test "should get new" do
    get new_envoye_url
    assert_response :success
  end

  test "should create envoye" do
    assert_difference("Envoye.count") do
      post envoyes_url, params: { envoye: { a: @envoye.a, brouillon: @envoye.brouillon, cc: @envoye.cc, cci: @envoye.cci, corbeille: @envoye.corbeille, date: @envoye.date, de: @envoye.de, message: @envoye.message, objet: @envoye.objet, user_id: @envoye.user_id } }
    end

    assert_redirected_to envoye_url(Envoye.last)
  end

  test "should show envoye" do
    get envoye_url(@envoye)
    assert_response :success
  end

  test "should get edit" do
    get edit_envoye_url(@envoye)
    assert_response :success
  end

  test "should update envoye" do
    patch envoye_url(@envoye), params: { envoye: { a: @envoye.a, brouillon: @envoye.brouillon, cc: @envoye.cc, cci: @envoye.cci, corbeille: @envoye.corbeille, date: @envoye.date, de: @envoye.de, message: @envoye.message, objet: @envoye.objet, user_id: @envoye.user_id } }
    assert_redirected_to envoye_url(@envoye)
  end

  test "should destroy envoye" do
    assert_difference("Envoye.count", -1) do
      delete envoye_url(@envoye)
    end

    assert_redirected_to envoyes_url
  end
end
