require "test_helper"

class RecusControllerTest < ActionDispatch::IntegrationTest
  setup do
    @recu = recus(:one)
  end

  test "should get index" do
    get recus_url
    assert_response :success
  end

  test "should get new" do
    get new_recu_url
    assert_response :success
  end

  test "should create recu" do
    assert_difference("Recu.count") do
      post recus_url, params: { recu: { a: @recu.a, cc: @recu.cc, cci: @recu.cci, corbeille: @recu.corbeille, date: @recu.date, de: @recu.de, message: @recu.message, objet: @recu.objet, user_id: @recu.user_id } }
    end

    assert_redirected_to recu_url(Recu.last)
  end

  test "should show recu" do
    get recu_url(@recu)
    assert_response :success
  end

  test "should get edit" do
    get edit_recu_url(@recu)
    assert_response :success
  end

  test "should update recu" do
    patch recu_url(@recu), params: { recu: { a: @recu.a, cc: @recu.cc, cci: @recu.cci, corbeille: @recu.corbeille, date: @recu.date, de: @recu.de, message: @recu.message, objet: @recu.objet, user_id: @recu.user_id } }
    assert_redirected_to recu_url(@recu)
  end

  test "should destroy recu" do
    assert_difference("Recu.count", -1) do
      delete recu_url(@recu)
    end

    assert_redirected_to recus_url
  end
end
